import hospital.Department;
import hospital.Hospital;
import hospital.exception.RemoveException;
import hospital.healthpersonal.Employee;
import hospital.patient.Patient;

/**
 * This class is used to test all the function and structure of whole project.
 */
public class HospitalClient {

    public static void main(String[] args) {
        //Creates a new hospital.
        Hospital hospital = new Hospital("St. Olavs hospital");
        //Fills hospital with test data.
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Gets the department with index 0.
        Department department = hospital.getDepartments().get(0);
        //Gets the employee with index 0 that will be removed.
        Employee employeeToBeRemoved = department.getEmployees().get(0);
        //Creates a new patient that we will try to remove.
        Patient patientToBeRemoved = new Patient("Ole", "Ivers", "01234567890");

        //Tries to remove employee. Will be successfully.
        try {
            department.remove(employeeToBeRemoved);
        } catch (RemoveException removeException) {
            System.out.println(removeException.getMessage());
        }

        //Tries to remove patient. Will fail.
        try {
            department.remove(patientToBeRemoved);
        } catch (RemoveException removeException) {
            System.out.println(removeException.getMessage());
        }
    }
}
