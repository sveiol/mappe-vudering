import hospital.Department;
import hospital.Hospital;
import hospital.healthpersonal.Employee;
import hospital.healthpersonal.Nurse;
import hospital.healthpersonal.doctor.GeneralPractitioner;
import hospital.healthpersonal.doctor.Surgeon;
import hospital.patient.Patient;

/**
 * This class is used to store all the test data that is used in HospitalClient class.
 */
public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * This method is used to fill a hospital with testData.
     * @param hospital Hospital to be filled with test data.
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", "12059955621"));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", "12059955622"));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", "10039955621"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", "17106099621"));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", "12095555622"));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", "10059955621"));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", "15059966621"));
        emergency.getPatients().add(new Patient("Inga", "Lykke", "02094455621"));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", "18100555621"));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", "09010855621"));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", "16050855622"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", "30029955629"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", "18030055621"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", "10040055621"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", "17070755622"));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", "20079955621"));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", "17070056621"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", "19010355622"));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}
