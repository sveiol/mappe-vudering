package hospital.healthpersonal;

/**
 * Represents one person. Is the super class in this build.
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Constructor for the class Person.
     * @param firstName Firstname of person.
     * @param lastName Lastname of person.
     * @param socialSecurityNumber Social security number of person.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Gets firstname.
     * @return Firstname of person.
     */
    public String getFirtName() {
        return firstName;
    }

    /**
     * Sets firstname.
     * @param firstName Firstname to be changed to.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets lastname.
     * @return Lastname of person.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets lastname.
     * @param lastName Lastname to be changed to.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets personnummer / social security number.
     * @return Personnummer / social security number.
     */
    public String getPersonnummer() {
        return socialSecurityNumber;
    }

    /**
     * Sets personnummer / social security number.
     * @param socialSecurityNumber Social security number to be changed to.
     */
    public void setPersonnummer(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Gets full name.
     * @return Full name of this.person.
     */
    public String getFullNavn() {
        return this.firstName + " " + this.lastName;
    }

    /**
     * Used for printing the details of a person object.
     * @return String with all details.
     */
    @Override
    public String toString() {
        return "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + "'";
    }
}
