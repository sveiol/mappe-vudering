package hospital.healthpersonal.doctor;

import hospital.healthpersonal.Employee;
import hospital.patient.Patient;

/**
 * Represents one doctor at the hospital.
 */
abstract class Doctor extends Employee {

    /**
     * Constructor for the class Doctor.
     * @param firstName Doctors firstname.
     * @param lastName Doctors lastname.
     * @param socialSecurityNumber Doctors social security number.
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This method is abstract which means its not implemented in this class.
     * But is implemented in the Surgeon and GeneralPractitioner classes.
     * @param patient The patients we want to diagnose.
     * @param diagnose The diagnose itself.
     */
    public abstract void setDiagnosis(Patient patient, String diagnose);
}
