package hospital.healthpersonal.doctor;

import hospital.patient.Patient;

/**
 * Represents one surgeon at the hospital.
 */
public class Surgeon extends Doctor{

    /**
     * Constructor for the class Surgeon.
     * @param firstName Surgeons firstname.
     * @param lastName Surgeons lastname.
     * @param socialSecurityNumber Surgeons social security number.
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This method is used to set a patients diagnose.
     * @param patient The patients we want to diagnose.
     * @param diagnose The diagnose itself.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnose) {
        patient.setDiagnosis(diagnose);
    }
}
