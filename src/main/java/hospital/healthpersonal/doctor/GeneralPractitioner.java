package hospital.healthpersonal.doctor;

import hospital.patient.Patient;

/**
 * Represents one general practitioner at the hospital.
 */
public class GeneralPractitioner extends Doctor{

    /**
     * Constructor for the class GeneralPractitioner.
     * @param firstName General practitioners firstname.
     * @param lastName General practitioners lastname.
     * @param socialSecurityNumber General practitioners social security number.
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This method is used to set a patients diagnose.
     * @param patient The patients we want to diagnose.
     * @param diagnose The diagnose itself.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnose) {
        patient.setDiagnosis(diagnose);
    }
}
