package hospital.healthpersonal;

/**
 * Represents one employee at the hospital.
 */
public class Employee extends Person{

    /**
     * Constructor for the class Employee.
     * @param firstName Employees firstname.
     * @param lastName Employees lastname.
     * @param socialSecurityNumber Employees social security number.
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
