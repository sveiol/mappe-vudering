package hospital.exception;

/**
 * This class is used for creating RemoveException if a remove from employee list or patient list fails.
 */
public class RemoveException extends Exception {

    /**
     * Constructor for the class RemoveException.
     * @param message Message to be displayed as the reason it failed.
     */
    public RemoveException(String message) {
        super(message);
    }
}
