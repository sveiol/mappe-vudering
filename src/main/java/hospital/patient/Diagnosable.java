package hospital.patient;

/**
 * This interface is used to define a abstract method and is implemented in Surgeon and GeneralPractitioner.
 */
public interface Diagnosable {
    /**
     * Used to diagnose a patient. This method is abstract and is implemented in Surgeon and GeneralPractitioner.
     * @param diagnosis A string that is the diagnose itself.
     */
    void setDiagnosis(String diagnosis);
}
