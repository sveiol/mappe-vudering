package hospital.patient;

import hospital.healthpersonal.Person;

/**
 * Represents one patient at the hospital.
 */
public class Patient extends Person implements Diagnosable{

    private String diagnose;

    /**
     * Constructor for the class Patient.
     * @param firstName Patients firstname.
     * @param lastName Patients lastname.
     * @param socialSecurityNumber Patients social security number.
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        this.diagnose = "";
    }

    /**
     * Works as the setter for the patient object diagnose field.
     * @param diagnosis The diagnose given.
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnose = diagnosis;
    }

    /**
     * This method is used to show the fields of a patient object.
     * @return A string with all the fields of the object.
     */
    @Override
    public String toString() {
        return super.toString() + ", diagnose='" + diagnose + "'";
    }
}
