package hospital;

import java.util.ArrayList;

/**
 * Represents one hospital. One hospital can have many departments.
 */
public class Hospital {
    private String hospitalName;
    ArrayList<Department> departmentList;

    /**
     * Constructor for the class Hospital.
     * @param hospitalName Name of the hospital.
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departmentList = new ArrayList<>();
    }

    /**
     * Gets the hospital name.
     * @return The hospital name.
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets the list of departments.
     * @return The list of departments.
     */
    public ArrayList<Department> getDepartments() {
        return this.departmentList;
    }

    /**
     * Add a new department to the list.
     * @param department The department to be added.
     */
    public void addDepartment(Department department) {
        //burde sjekke at navnene ikke er like
        departmentList.add(department);
    }

    /**
     * This method is used to show the details about the hospital.
     * @return A string with all details about the hospital.
     */
    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departmentList=" + departmentList +
                '}';
    }
}
