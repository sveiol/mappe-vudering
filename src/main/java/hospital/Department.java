package hospital;

import hospital.exception.RemoveException;
import hospital.healthpersonal.Employee;
import hospital.patient.Patient;
import hospital.healthpersonal.Person;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents one department. Has two arrays, one for Employee and one for Patient.
 */
public class Department {
    private String departmentName;
    private ArrayList<Employee> departmentEmployeeList;
    private ArrayList<Patient> departmentPatientList;

    /**
     * Constructor for the class Department.
     * @param departmentName Name of the department
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        departmentEmployeeList = new ArrayList<>();
        departmentPatientList = new ArrayList<>();
    }

    /**
     * Gets the department name.
     * @return Department name.
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     * @param departmentName New name of department.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets the ArrayList of employees.
     * @return List of employees.
     */
    public ArrayList<Employee> getEmployees() {
        return this.departmentEmployeeList;
    }

    /**
     * Used for adding new employees to the list.
     * @param employee Employee to be added.
     */
    public void addEmployee(Employee employee) {
        boolean flag = false;
        int index = 0;
        while (index < departmentEmployeeList.size() && !flag) {
            if (employee.getPersonnummer().equals(departmentEmployeeList.get(index).getPersonnummer())) {
                flag = true;
            }
            index++;
        }
        if (!flag) {
            this.departmentEmployeeList.add(employee);
        } else {
            System.out.println("Social security number already in registry, unable to add current employee.");
        }
    }

    /**
     * Gets the ArrayList of patients.
     * @return List of patients.
     */
    public ArrayList<Patient> getPatients() {
        return this.departmentPatientList;
    }

    /**
     * Used for adding new patient to the list.
     * @param patient Patient to be added.
     */
    public void addPatient(Patient patient) {
        boolean flag = false;
        int index = 0;
        while (index < departmentPatientList.size() && !flag) {
            if (patient.equals(departmentPatientList.get(index))) {
                flag = true;
            }
            index++;
        }
        if (!flag) {
            this.departmentPatientList.add(patient);
        } else {
            System.out.println("Social security number already in registry, unable to add current employee.");
        }
    }

    /**
     * Used for removing either employee form employees list or patient from patients list.
     * @param person Person to be removed.
     * @throws RemoveException Exception to be thrown if not removed.
     */
    public void remove(Person person) throws RemoveException {
        if (person == null) {
            throw new RemoveException("Failed to remove. Object cannot be null.");
        }
        boolean flag = false;
        int index = 0;
        if (person instanceof Employee) {
            while (!flag && index < departmentEmployeeList.size()) {
                if (person.equals(departmentEmployeeList.get(index))) {
                    flag = true;
                    departmentEmployeeList.remove(index);
                }
                index++;
            }
            if (!flag) {
                throw new RemoveException("Failed to remove. Person most likely not in registry.");
            }
        } else if (person instanceof Patient) {
            while (!flag && index < departmentPatientList.size()) {
                if (person.equals(departmentPatientList.get(index))) {
                    flag = true;
                    departmentPatientList.remove(index);
                }
                index++;
            }
            if (!flag) {
                throw new RemoveException("Failed to remove. Person most likely not in registry.");
            }
        } else {
            throw new RemoveException("Failed to remove. Unknown class.");
        }
    }

    /**
     * Used for printing the details of a department object.
     * @return String with all details.
     */
    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", departmentEmployeeList=" + departmentEmployeeList +
                ", departmentPatientList=" + departmentPatientList +
                '}';
    }

    /**
     * Used for comparing two objects.
     * @param o Object to be compared.
     * @return True or False, if equal return true else false.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) && Objects.equals(departmentEmployeeList, that.departmentEmployeeList) && Objects.equals(departmentPatientList, that.departmentPatientList);
    }

    /**
     * Creates a hashCode based on departmentName, departmentEmployeeList and departmentPatientList.
     * @return Hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName, departmentEmployeeList, departmentPatientList);
    }
}
