import hospital.Department;
import hospital.exception.RemoveException;
import hospital.healthpersonal.Employee;
import hospital.patient.Patient;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests to be performed:
 *
 * Positive tester:
 *  - Check to see if the remove method works with correct input and removes objects from list.
 *  - Need to test both Employee list and Patient list.
 *
 *  Negative tester:
 *  -Check to see if the remove method works with incorrect input, and how it handles exceptions.
 *  - Need to test both Employee list and Patient list.
 */

public class RemoveTest {

    @Test
    @DisplayName("Tests the remove() from employees list with correct input.")
    public void testEmployeesListWithCorrectUse() throws RemoveException {
        //Creates a new department.
        Department department = new Department("Akutten");
        //Creates a new employee.
        Employee employee1 = new Employee("Inco", "Gnito",
                "01234567890");
        //Creates a new employee.
        Employee employee2 = new Employee("Anton", "Iversen",
                "51234567891");
        //adds two employees to list.
        department.addEmployee(employee1);
        department.addEmployee(employee2);

        //test for adding new employees to the list.
        assertEquals(2, department.getEmployees().size());

        //removes one employee from the list.
        department.remove(employee1);
        //test to see if size of array is changed.
        assertEquals(1, department.getEmployees().size());
    }

    @Test
    @DisplayName("Tests the remove() from patients list with correct input.")
    public void testPatientsListWithCorrectUse() throws RemoveException {
        //Creates a new department.
        Department department = new Department("Akutten");
        //Creates a new patient.
        Patient patient1 = new Patient("Geir", "Bakke",
                "52334467991");
        //Creates a new patient.
        Patient patient2 = new Patient("Ola", "Laugen",
                "52334467991");
        //adds two patients to list.
        department.addPatient(patient1);
        department.addPatient(patient2);

        //test for adding new patients to the list.
        assertEquals(2, department.getPatients().size());

        //removes one patient from the list.
        department.remove(patient2);
        //test to se if size of array is changed.
        assertEquals(1, department.getPatients().size());
    }

    @Test
    @DisplayName("Tests the remove() from employees list with incorrect use.")
    public void testEmployeesListWithIncorrectUse() {
        //Creates a new department.
        Department department = new Department("Akutten");
        //Creates a new employee.
        Employee employee1 = new Employee("Ivar", "Berg",
                "11224567893");
        //Creates a new employee.
        Employee employee2 = new Employee("Anton", "Iversen",
                "51125167891");
        //Creates a new employee.
        Employee employee3 = new Employee("Oliver", "Knudsen",
                "15081367893");
        //adds two employees to list
        department.addEmployee(employee1);
        department.addEmployee(employee2);

        //test for exception handling when you try remove a object that isn't in th list.
        Throwable exception1 = assertThrows(RemoveException.class, () -> department.remove(employee3));
        assertEquals("Failed to remove. Person most likely not in registry.", exception1.getMessage());

        //test for exception handling when you try to remove null.
        Throwable exception2 = assertThrows(RemoveException.class, () -> department.remove(null));
        assertEquals("Failed to remove. Object cannot be null.", exception2.getMessage());
    }

    @Test
    @DisplayName("Tests the remove() from patients list with incorrect use.")
    public void testPatientsListWithIncorrectUse() {
        //Creates a new department.
        Department department = new Department("Akutten");
        //Creates a new patient.
        Patient patient1 = new Patient("Geir", "Bakke",
                "52334467991");
        //Creates a new patient.
        Patient patient2 = new Patient("Ola", "Laugen",
                "52334467991");
        //Creates a new patient.
        Patient patient3 = new Patient("Oliver", "Iversen",
                "15081367893");
        //adds two patients to list
        department.addPatient(patient1);
        department.addPatient(patient2);

        //test for exception handling when you try remove a object that isn't in th list.
        Throwable exception1 = assertThrows(RemoveException.class, () -> department.remove(patient3));
        assertEquals("Failed to remove. Person most likely not in registry.", exception1.getMessage());

        //test for exception handling when you try to remove null.
        Throwable exception2 = assertThrows(RemoveException.class, () -> department.remove(null));
        assertEquals("Failed to remove. Object cannot be null.", exception2.getMessage());
    }
}